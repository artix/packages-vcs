# Maintainer: artoo <artoo@artixlinux.org>

_pkgbase=artools
_branch=arches

pkgbase=artools-git
pkgname=('artools-base-git'
        'artools-pkg-git'
        'artools-iso-git')
pkgver=0.31.1.r3.g1c447d2
pkgrel=1
arch=('any')
pkgdesc='Development tools for Artix'
license=('GPL')
url='https://gitea.artixlinux.org/artix/artools'
makedepends=('git')
source=("git+$url.git#branch=$_branch")
sha256sums=('SKIP')

prepare() {
    cd ${_pkgbase}
    # patches here
}

pkgver() {
    cd ${_pkgbase}
    git describe --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

build() {
    make -C ${_pkgbase} PREFIX=/usr BUILDTOOLVER="${pkgver}-${pkgrel}-${arch}"
}

package_artools-base-git() {
    pkgdesc='Development tools for Artix (base tools)'
    provides=("artools-base-git=$pkgver")
    depends=('awk' 'bash' 'coreutils' 'grep'
            'pacman' 'util-linux')
    optdepends=('artools-pkg: Artix package tools'
                'artools-iso: Artix iso tools')
    conflicts=('artools-base')
    backup=('etc/artools/artools-base.conf')

    make -C ${_pkgbase} PREFIX=/usr DESTDIR=${pkgdir} install_base
}

package_artools-pkg-git() {
    pkgdesc='Development tools for Artix (packaging tools)'
    provides=("artools-pkg-git=$pkgver")
    depends=('artools-base-git' 'awk' 'parallel' 'bash' 'rsync' 'go-yq'
            'openssh' 'diffutils' 'findutils' 'grep' 'sed' 'util-linux' 'binutils')
    conflicts=('artools-pkg')
    backup=('etc/artools/artools-pkg.conf')

    make -C ${_pkgbase} PREFIX=/usr DESTDIR=${pkgdir} install_pkg
}

package_artools-iso-git() {
    pkgdesc='Development tools for Artix (ISO tools)'
    provides=("artools-iso-git=$pkgver")
    depends=('artools-base-git' 'iso-profiles' 'squashfs-tools'
            'grub' 'dosfstools' 'libisoburn' 'bash' 'dosfstools'
            'e2fsprogs' 'libarchive' 'mtools')
    conflicts=('artools-iso')
    backup=('etc/artools/artools-iso.conf')

    make -C ${_pkgbase} PREFIX=/usr DESTDIR=${pkgdir} install_iso
}
