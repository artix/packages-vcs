# Maintainer: Muhammad Herdiansyah <koni@artixlinux.org>

_alpm=1.7

pkgbase=dinit
pkgname=('dinit-base' 'dinit')
pkgver=0.18.0
pkgrel=5
pkgdesc="Service monitoring/init system"
arch=('x86_64')
url="https://github.com/davmac314/dinit"
license=('Apache-2.0')
depends=('glibc' 'gcc-libs')
makedepends=('git' 'meson')
source=("git+https://github.com/davmac314/dinit.git#tag=v$pkgver"
        "git+https://gitea.artixlinux.org/artix/alpm-hooks.git#tag=$_alpm"
        "dinit-init"
        0001-meson-fix-poweroff.patch)
sha256sums=('fc0a64f5029f5fb955e29615277ec0fdfe6d761befa4f5a146240ee80c794390'
            '6b89db32c61731ae970a7043907c08c51df3aa6b0fb9a527e70a2b6346150511'
            '6a51b4c71bbe9ac362f36debda9a6450afbef035aeb4588eed5913f7e589666b'
            '525cf0f6eb2e0c99cc7a5b0a20455d5d7b4f7d264d61d1d679df80c8be0a13b6')

prepare(){
    git -C "$pkgbase" apply ../0001-meson-fix-poweroff.patch
}

build() {
    local _meson_options=()
    _meson_options+=(
        -Ddinit-sbindir=/usr/bin
        -Dsupport-cgroups=enabled
        -Dbuild-shutdown=enabled
        -Dunit-tests=true
        -Digr-tests=true
    )
    artix-meson "$pkgbase" build "${_meson_options[@]}"
    meson compile -C build
}

check() {
    meson test -C build --print-errorlogs
}

package_dinit-base() {
    pkgdesc='Service monitoring/init system -- base package'
    install=dinit.install

    meson install -C build --destdir "$pkgdir"

    install -d "$srcdir"/_dinit/usr/{bin,share/man/man8}

    mv -v "$pkgdir"/usr/bin/{halt,reboot,shutdown,poweroff} \
        "$srcdir"/_dinit/usr/bin/
    mv -v "$pkgdir"/usr/share/man/man8/{halt,reboot,shutdown,poweroff}.8 \
        "$srcdir"/_dinit/usr/share/man/man8/
}

package_dinit() {
    pkgdesc='Service monitoring/init system -- init package'
    depends+=('sh' 'dinit-base' 'dinit-rc')
    provides=('svc-manager')
    conflicts=('svc-manager')

    mv -v "$srcdir"/_dinit/usr "$pkgdir"/

    # dinit-init symlink
    install -Dm755 "$srcdir/dinit-init" "$pkgdir/usr/bin/dinit-init"

    make -C "$srcdir/alpm-hooks" DESTDIR="$pkgdir" install_dinit
}



